from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, "input.html")

def cekhuruf(request):
    vokal = "AaEeIiOoUu"
    text = request.POST['text']
    lowtext =  text.lower()
    hasil = {each for each in lowtext if each in vokal}
    hasilakhir = text + " = " + str(len(hasil)) + " yaitu " + str(hasil)
    return render(request, "hasil.html", {"hasil": hasilakhir})
