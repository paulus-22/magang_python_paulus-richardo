from django import forms
from .models import user, buku

class RegisterForm(forms.ModelForm):
    class Meta:
        model = user
        fields = ['nama', 'email', 'password', 'no_telepon', 'alamat', 'roles']
        widgets = {
            'password': forms.PasswordInput(),
        }

class LoginForm(forms.Form):
    email = forms.EmailField(label="Email")
    password = forms.CharField(label="Password", max_length=1024, widget=forms.PasswordInput())

class BookForm(forms.ModelForm):
    class Meta:
        model = buku
        fields = ['judul_buku', 'gambar', 'deskripsi', 'genre', 'publish', 'harga']
