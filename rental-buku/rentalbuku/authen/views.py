from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.http.request import HttpRequest
from django.http.response import Http404, HttpResponse, HttpResponseForbidden
from django.shortcuts import redirect, render, get_object_or_404
from django.urls.base import reverse
from .forms import BookForm, RegisterForm, LoginForm
from .models import user, buku, genre
from django.contrib.auth.decorators import login_required
# Create your views here.

def register(request: HttpRequest):
    success = ""
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            nama = form.cleaned_data['nama']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            no_telepon = form.cleaned_data['no_telepon']
            alamat = form.cleaned_data['alamat']
            roles = form.cleaned_data['roles']
            user.objects.create_user(email, password, nama, roles, no_telepon, alamat)
            success = "Success"
    else:
        form = RegisterForm()
    return render(request, 'register.html', {'form': form, 'msg': success})

def login(request: HttpRequest):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = authenticate(request, username=email, password=password)
            response = HttpResponse()
            if user is not None:
                auth_login(request, user)
                return redirect(reverse('home'))
            else:
                response.write('Login Gagal')
                return response
    else:
        form = LoginForm()
    return render(request, 'login.html', {'form': form})

def logout(request: HttpRequest):
    auth_logout(request)
    return redirect(reverse('login'))

def user_menu(user: user):
    menus = {'Logout': reverse('logout')}
    if user.roles.id == 1:
        menus['Buku saya'] =  reverse('my_book')
        menus['Peminjaman saya'] =  reverse('borrowed_book')
        menus['Tambah buku'] =  reverse('add_book')
    elif user.roles.id == 2:
        menus['Pinjaman saya'] =  reverse('borrow_book')
    return menus

def peminjam(user: user):
    return user.roles.id == 2

def pemilik(user: user):
    return user.roles.id == 1

def detail_book(request: HttpRequest, buku_id: int):
    info = get_object_or_404(buku, pk=buku_id)
    menus = user_menu(request.user)
    return render(request, 'detail_book.html', {'menus': menus, 'info': info})

@login_required
def delete_book(request: HttpRequest, buku_id: int):
    book = get_object_or_404(buku, pk=buku_id)
    if not pemilik(request.user) and book.pemilik != request.user:
        return HttpResponseForbidden()
    book.delete()
    return redirect(reverse('detail_book', kwargs={'buku_id': buku_id}))

@login_required
def update_book(request: HttpRequest, buku_id: int):
    menus = user_menu(request.user)
    book = get_object_or_404(buku, pk=buku_id)
    if not pemilik(request.user) and book.pemilik != request.user:
        return HttpResponseForbidden()
    if request.method == "POST":
        form = BookForm(request.POST, request.FILES, instance=book)
        if form.is_valid():
            book.save()
            return redirect(reverse('detail_book', kwargs={'buku_id': buku_id}))
    else:
        form = BookForm(instance=book)
    return render(request, 'update_book.html', {'menus': menus, 'form': form, 'id': book.id})

@login_required
def add_book(request: HttpRequest):
    menus = user_menu(request.user)
    if not pemilik(request.user):
        return HttpResponseForbidden()
    if request.method == "POST":
        form = BookForm(request.POST, request.FILES)
        if form.is_valid():
            book = form.save(commit=False)
            book.pemilik = request.user
            book.save()
            return redirect(reverse('my_book'))
    else:
        form = BookForm()
    return render(request, 'add_book.html', {'menus': menus, 'form': form})

@login_required
def my_book(request: HttpRequest):
    if not pemilik(request.user):
        return HttpResponseForbidden()
    menus = user_menu(request.user)
    books = buku.objects.filter(pemilik=request.user.id)
    return render(request, 'books.html', {'books':books, 'menus':menus})

@login_required
def borrowed_book(request: HttpRequest):
    if not pemilik(request.user):
        return HttpResponseForbidden()
    menus = user_menu(request.user)
    books = buku.objects.filter(pemilik=request.user.id, status=True)
    return render(request, 'books.html', {'books':books, 'menus':menus})

@login_required
def borrow_book(request: HttpRequest):
    if not peminjam(request.user):
        return HttpResponseForbidden()
    menus = user_menu(request.user)
    books = buku.objects.filter(peminjam=request.user.id)
    return render(request, 'books.html', {'books':books, 'menus':menus})

@login_required
def home(request: HttpRequest):
    menus = user_menu(request.user)
    content = {}
    if request.user.roles.id == 1:
        borrowed_book = len(buku.objects.filter(pemilik=request.user.id, status=True))
        my_book = len(buku.objects.filter(pemilik=request.user.id))
        content = {'buku yang anda punya':my_book, 'buku anda yang dipinjam': borrowed_book}
    elif request.user.roles.id == 2:
        borrow_book = len(buku.objects.filter(peminjam=request.user.id))
        content = {'buku yang anda pinjam': borrow_book}
    return render(request, 'home.html', {'content': content, 'menus':menus})
