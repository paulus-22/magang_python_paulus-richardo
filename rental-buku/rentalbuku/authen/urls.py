from django.urls import path
from . import views
from django.contrib.auth import logout, views as auth_views

urlpatterns = [
    path('register', views.register, name='register'),
    path('logout', views.logout, name='logout'),
    path('login', views.login, name='login'),
    path('home', views.home, name='home'),
    path('my-book', views.my_book , name='my_book'),
    path('borrowed-book', views.borrowed_book, name='borrowed_book'),
    path('borrow-book', views.borrow_book, name='borrow_book'),
    path('add-book', views.add_book, name='add_book'),
    path('books/<int:buku_id>', views.detail_book, name='detail_book'),
    path('books/<int:buku_id>/update', views.update_book, name='update_book'),
    path('books/<int:buku_id>/delete', views.delete_book, name='delete_book'),
]
