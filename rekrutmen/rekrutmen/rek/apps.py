from django.apps import AppConfig


class RekConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rek'
