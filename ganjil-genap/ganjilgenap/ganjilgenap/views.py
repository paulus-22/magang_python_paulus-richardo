from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, "input.html")


def ganjilgenap(request):

    num1 = request.POST['num1']
    num2 = request.POST['num2']

    if num1.isdigit() and num2.isdigit():
        a = int(num1)
        b = int(num2)
        res = []
        angka = "Angka "
        genap = " adalah genap"
        ganjil = " adalah ganjil"
        for i in range(a,b+1):
            if (i%2)==0:
                res.append(angka + str(i) + genap)
            else:
                res.append(angka + str(i) + ganjil)
        return render(request, "result.html", {"result": res})
    else:
        res = "Angka tidak valid"
        return render(request, "result.html", {"result": res})
