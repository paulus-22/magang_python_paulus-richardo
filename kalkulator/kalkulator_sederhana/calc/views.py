from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, "input.html")

def calc(request):
    input = (request.POST['input']).lower()
    stack, curr, op = [], 0, "+"
    all_op = {"+","-","*","/"}
    nums = set(str(x) for x in range(10))
    result = ""

    for index in range(len(input)):
        char = input[index]

        if char in nums:
            curr = curr * 10 + int(char)

        if char in all_op or index == len(input) - 1:
            if op == "+":
                stack.append(curr)
            elif op == "-":
                stack.append(-curr)
            elif op == "*":
                stack[-1] *= curr
            elif op == "/":
                try:
                    stack[-1] = int(stack[-1]/curr)
                except ZeroDivisionError:
                    result = "tidak bisa dilakukan"
            curr = 0
            op = char
    if result == "tidak bisa dilakukan" :
        result = "tidak bisa dilakukan"
    else:
        result=sum(stack)
    return render(request, "input.html", {"result": result})
