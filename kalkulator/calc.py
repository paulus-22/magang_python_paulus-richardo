def calcu():
    strparam = input("Kalkulator = ")
    stack, curr, operator = [], 0, "+"
    all_op = {"+","-","*","/"}
    nums = set(str(x) for x in range(10))

    for index in range(len(strparam)):
        char = strparam[index]

        if char in nums:
            curr = curr * 10 + int(char)

        if char in all_op or index == len(strparam) - 1:
            if operator == "+":
                stack.append(curr)
            elif operator == "-":
                stack.append(-curr)
            elif operator == "*":
                stack[-1] *= curr
            elif operator == "/":
                try:
                    stack[-1] = int(stack[-1]/curr)
                except ZeroDivisionError:
                    result = "error bang"

            curr = 0
            operator = char
    if result == "error bang":
        print(result)
    else :
        result=sum(stack)
        print(result)

    return sum(stack)

calcu()
